#include "csapp.h"
#include <stdio.h>

void echo(int connfd);

int main(int argc, char **argv)
{
	int listenfd, connfd;
	unsigned int clientlen;
	struct sockaddr_in clientaddr;
	struct hostent *hp;
	char *haddrp, *port;

	if (argc != 2) {
		fprintf(stderr, "usage: %s <port>\n", argv[0]);
		exit(0);
	}
	port = argv[1];

	listenfd = Open_listenfd(port);
	while (1) {
		clientlen = sizeof(clientaddr);
		connfd = Accept(listenfd, (SA *)&clientaddr, &clientlen);

		/* Determine the domain name and IP address of the client */
		hp = Gethostbyaddr((const char *)&clientaddr.sin_addr.s_addr,
					sizeof(clientaddr.sin_addr.s_addr), AF_INET);
		haddrp = inet_ntoa(clientaddr.sin_addr);
		printf("server connected to %s (%s)\n", hp->h_name, haddrp);

		echo(connfd);
		Close(connfd);
	}
	exit(0);
}

void echo(int connfd)
{
	size_t n;
	struct stat sbuf;
	char fileSize[15];
	char fileName[50];
	char aEscribir[40];
	char buf[MAXLINE];
	rio_t rio;
	char buff[256];
	char c;
	int rfa;
	Rio_readinitb(&rio, connfd);
	while((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0) {
		strncpy(fileName,buf,strlen(buf));
		printf("\nArchivo a leer: %s", fileName);
		if (stat(strtok(fileName,"\n"), &sbuf) < 0) {
			puts("El archivo no existe...");
			printf("%s",fileName);
			return;
		}
		while(1){
			printf("\nIngrese 'y' para continuar: ");
			rfa=scanf("%c",&c);
			if(c=='y'){
				strncpy(aEscribir, "El tamaño del archivo es: ",27);
				sprintf(fileSize, "%li",sbuf.st_size);
				strtok(aEscribir,"(8)\n");//xq en la cadena se genera un (8) al final.
				strcat(aEscribir, fileSize);			
				strcat(aEscribir, " bytes...\n");
				strncpy(buf, aEscribir,strlen(aEscribir));
				break;
			}
			break;	
		}
		Rio_writen(connfd, buf, strlen(buf));
		int fd;
		fd=open(fileName,O_RDONLY);
		rfa=read(fd,&buff[0],256);
		close(fd);
		break;	
					
	}
	while((n = Rio_readlineb(&rio, buf, MAXLINE)) != 0) {
		puts("\nCliente dice: \n");
		puts(buf);		
		if(strcmp(buf,"Enviame el archivo...\n")==0){
			strcpy(buf,buff);
			Rio_writen(connfd,buf,strlen(buf));
			puts("\nArchivo enviado...");		
		}	
					
	}
	puts("\n\nFinaliza la conexión con este cliente...\n\nEsperando por una nueva conexion");
}
