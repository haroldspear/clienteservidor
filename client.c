#include "csapp.h"


int main(int argc, char **argv)
{
	int clientfd;
	char *port;
	char *host, buf[MAXLINE];
	rio_t rio;
	char fileName[256];
	char conf;
	int rfa;
	if (argc != 4) {
		fprintf(stderr, "usage: %s <host> <port>\n", argv[0]);
		exit(0);
	}
	host = argv[1];
	port = argv[2];
	clientfd = Open_clientfd(host, port);
	Rio_readinitb(&rio, clientfd);
	while(1){
		strncpy(fileName,argv[3],strlen(argv[3]));
		strcat(fileName,"\n");
		strncpy(buf, fileName,strlen(fileName)+2);
		break;	
	}
	Rio_writen(clientfd, buf, strlen(buf));
	while(Rio_readlineb(&rio, buf, MAXLINE)!=0){
		strncpy(buf,"Enviame el archivo...\n",23);
		Rio_writen(clientfd, buf, strlen(buf));
		break;
	}	
	int fd;
	while(Rio_readlineb(&rio, buf, MAXLINE)!=0){
		printf("\nSe copiarà el archivo... \n\nIngrese 'y' para continuar: ");		
		rfa=scanf("%c",&conf);
		if(conf=='y'){
			char copiaFilename[263];
			strcpy(copiaFilename,"copia_");
			strcat(copiaFilename,fileName);
			fd=open(copiaFilename, O_CREAT|O_WRONLY|O_APPEND,S_IRWXO|S_IRWXU|S_IRWXG);
			rfa=write(fd,&buf[0],strlen(buf));		
			close(fd);
			puts("\nSe copiò el archivo..");
			break;
		}
	}
	Close(clientfd);
	exit(0);
}
